// base action declaration, each action needs to have a type
declare interface IAction {
  type: string;
}

declare interface IComponentAction extends IAction {
  component: React$Component<*,*,*>;
}
