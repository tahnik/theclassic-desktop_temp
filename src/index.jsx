import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './js/components/app';

// the element used to inject react at runtime
const element = document.getElementById('root');
// a simple replacer function used to inject components at runtime
const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Component/>
    </AppContainer>,
    element
  );
};

// first application render call
render(App);

// hot module reload logic
if(module.hot) {
  module.hot.accept('./js/components/app', () => {
    render(App);
  });
}
