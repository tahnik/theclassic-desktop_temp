import { combineReducers } from 'redux';
import componentReducer from './component-reducer';

export default combineReducers({
  componentReducer,
});
