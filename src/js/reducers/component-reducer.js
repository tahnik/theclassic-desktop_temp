import constants from '../constants/action-types';

function registerComponent(state: IState = {}, action: IComponentAction): IState {
  switch (action.type) {
    case constants.ACTION_REGISTER_COMPONENT: {
      // already loaded components
      const components = [...state.components, action.component];
      return {
        ...state,
        components,
      };
    }
    default: {
      return state;
    }
  }
}
export default registerComponent;
