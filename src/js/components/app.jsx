import React from 'react';

class App extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <h1>Hello World</h1>
        <h2>Fine!</h2>
      </div>
    );
  }
}
export default App;
