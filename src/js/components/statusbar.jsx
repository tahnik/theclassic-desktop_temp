import React from 'react';

class StatusBar extends React.Component {

  props: {
    message: ?string;
  }

  constructor(props: Object) {
    super(props)
  }

  render() {
    return (
      <footer>
        <h1>{this.props.message}</h1>
      </footer>
    );
  }
}
export default StatusBar;
