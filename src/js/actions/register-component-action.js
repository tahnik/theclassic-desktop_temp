import constants from '../constants/action-types';

function registerComponent(component: React$Component<*, *, *>) {
  return {
    type: constants.ACTION_REGISTER_COMPONENT,
    component,
  };
}

export default registerComponent;
