import constants from '../constants/action-types';

function nextTrack() {
  return {
    type: constants.ACTION_PLAYER_NEXT,
  };
}

function previousTrack() {
  return {
    type: constants.ACTION_PLAYER_PREVIOUS,
  };
}

export default {
  nextTrack,
  previousTrack,
};
