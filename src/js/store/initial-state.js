const state: IState = {
  // holds all plugin components
  components: [],
  // holds all current toast notifications
};
export default state;
