/* eslint-env jest */
import constants from 'js/constants/action-types';
import registerComponent from 'js/actions/register-component-action';
import component from 'js/components/statusbar';

describe('(Action) RegisterComponentAction', () => {
  it('creates a valid action object', () => {
    const expected = {
      type: constants.ACTION_REGISTER_COMPONENT,
      component,
    };

    const result = registerComponent(component);
    expect(expected).toEqual(result);
  });
});
