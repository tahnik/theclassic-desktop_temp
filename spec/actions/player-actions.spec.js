/* eslint-env jest */
import constants from 'js/constants/action-types';
import actions from 'js/actions/player-actions';

describe('(Action) PlayerActions', () => {
  it('plays the next song', () => {
    const expected = { type: constants.ACTION_PLAYER_NEXT };
    expect(expected).toEqual(actions.nextTrack());
  });

  it('plays the previous song', () => {
    const expected = { type: constants.ACTION_PLAYER_PREVIOUS };
    expect(expected).toEqual(actions.previousTrack());
  });
});
