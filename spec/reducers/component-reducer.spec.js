/* eslint-env jest */
import constants from 'js/constants/action-types';
import reducer from 'js/reducers/component-reducer';
import component from 'js/components/statusbar';

// the initial state used for testing only!
const initialState = {
  components: [],
};

describe('(Reducer) ComponentReducer', () => {
  // this test is to ensure that each reducer only responds to its actions
  // not sure if needed, but it makes sure that each reducer has a default switch
  // statement implemented.
  it('returns the initial state if action doesn\'t match', () => {
    const action = { type: 'unknown' };
    const newState = reducer(initialState, action);

    expect(newState).toEqual(initialState);
  });

  it('registers a new component', () => {
    const action = {
      type: constants.ACTION_REGISTER_COMPONENT,
      component,
    };

    const newState = reducer(initialState, action);
    expect(newState).toEqual({ components: [component] });
  });
});
