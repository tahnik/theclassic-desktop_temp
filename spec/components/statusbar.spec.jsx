/* eslint-env jest */
import { shallow } from 'enzyme';
import React from 'react';
import StatusBar from 'js/components/statusbar';

// message used for testing
const testMessage = "Test Message";

describe('(Component) StatusBar', () => {
  it('should display message', () => {
    // only shallow render is needed for this component
    const element = <StatusBar message={ testMessage }/>;
    const wrapper = shallow(element);
    expect(wrapper.find('h1').text()).toEqual(testMessage);
  });
});
